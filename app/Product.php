<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['item_id','name', 'shipping_address', 'shipping_code', 'price'];

    public function item()
    {
      return $this->belongsTo('App\Item');
    }

}
