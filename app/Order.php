<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = ['user_id','order_number', 'total'];
    protected $dates = ['paid_at'];

    public function item()
    {
      return $this->belongsTo('App\Item');
    }
}
