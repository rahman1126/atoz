<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Order;
use Auth;

use Carbon\Carbon;

class OrderController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index()
  {

    $orders = Order::where('user_id', Auth::id())->orderBy('created_at', 'DESC')->paginate(20);
    return view('orders')
      ->with('orders', $orders);
  }

  public function search(Request $request)
  {

    if ($request->get('key') != null) {
      if (strlen($request->get('key') < 10)) {
        $orders = Order::where('user_id', Auth::id())->where('order_number', 'like', $request->get('key'). '%')->orderBy('created_at', 'DESC')->paginate(20);
      } else {
        $orders = Order::where('user_id', Auth::id())->where('order_number', $request->get('key'))->orderBy('created_at', 'DESC')->paginate(20);
      }
    } else {
      $orders = Order::where('user_id', Auth::id())->orderBy('created_at', 'DESC')->paginate(20);
    }

    return view('orders')
      ->with('orders', $orders);
  }
}
