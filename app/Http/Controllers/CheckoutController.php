<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use Auth;
use Carbon\Carbon;
use Session;

use App\Order;
use App\Product;
use App\Pulsa;
use App\Item;

class CheckoutController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getCheckout()
    {
      return view('orders.payment');
    }

    public function postCheckout(Request $request)
    {

      $valid = Validator::make($request->all(), [
        'orderNumber' => 'exists:orders,order_number',
      ]);

      if ($valid->fails()) {
        return redirect()->back()
          ->withErrors($valid)
          ->withInput();
      } else {

        $order = Order::where('order_number', $request->input('orderNumber'))->first();

        if ($order->user_id != Auth::id()) {

          return redirect()->back()
            ->with('error', 'This order is not belong to you');

        } elseif ($order->status != 0) {

          return redirect()->back()
            ->with('error', 'This order has been canceled or paid');

        } else {

          if($order->created_at->addMinute(5)->toDateTimeString() <= Carbon::now()){
            $order->status = 2;
            $order->save();

            return redirect()->back()
              ->with('error', 'Order already canceled automatically because older than 5 minutes.');
          }

          $order->status = 1;
          $order->paid_at = date('Y-m-d H:i:s');

          if ($order->save()) {

            $this->processOrder($order->item_id);

            Session::forget('order_id');

            // do the process
            return redirect('order')
              ->with('success', 'Order has been paid, and processed');

          } else {

            return redirect()
              ->back()
              ->with('error', 'Something went wrong, please try again');

          }

        }

      }

    }


    protected function processOrder($itemId)
    {
      $item = Item::find($itemId);
      if ($item->type == 'pulsa') {

        $status = '';
        $dt = Carbon::now();

        if ($dt->hour >= '9' && $dt->hour <= '17') {
          $chance = rand(1, 10);
          if ($chance > 1) {
            $status = 'success';
          } else {
            $status = 'fail';
          }
        } else {
          $chance = rand(1, 10);
          if ($chance > 6) {
            $status = 'success';
          } else {
            $status = 'fail';
          }
        }

        $item->pulsa->balance_status = $status;

        if ($item->pulsa->save()) {
          return true;
        } else {
          return false;
        }

      } elseif ($item->type == 'product') {

        $item->product->shipping_code = $this->generateOrderNumber();

        if ($item->product->save()) {
          return true;
        } else {
          return false;
        }

      }
    }

    protected function generateOrderNumber()
    {
      $on = mt_rand(11111111, 99999999);
      $check_on = Product::where('shipping_code', $on)->first();

      while ($check_on == $on)
      {
          $on = mt_rand(11111111, 99999999);
          $check_on = Order::where('shipping_code', $on)->first();
      }

      return $on;

    }
}
