<?php

namespace App\Http\Controllers\Item;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Validator;
use Session;

use App\Product;
use App\Order;
use App\Item;

class ProductController extends Controller
{

    protected $itemFee = 10000;
    protected $itemType = 'product';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
      return view('items.product.buy');
    }

    /*
    * Make order
    */
    public function makeOrder(Request $request)
    {
      $valid = Validator::make($request->all(), [
        'productName' => 'required|string|min:10|max:150',
        'shippingAddress' => 'required|string|min:10|max:150',
        'price' => 'required|numeric',
      ]);

      if ($valid->fails()) {

        return redirect()->back()
          ->withErrors($valid)
          ->withInput();

      } else {

        $item = new Item;
        $item->user_id = Auth::id();
        $item->type = $this->itemType;

        if ($item->save()) {

          $product = new Product;
          $product->item_id = $item->id;
          $product->name = $request->input('productName');
          $product->shipping_address = $request->input('shippingAddress');
          $product->price = $request->input('price');

          if ($product->save()) {

            $order = new Order;
            $order->user_id = Auth::id();
            $order->order_number = $this->generateOrderNumber();
            $order->item_id = $product->item_id;
            $order->total = $request->input('price') + $this->itemFee;

            if ($order->save()) {

              Session::put('order_id', $order->id);
              return redirect('product-success')
                ->with('success', 'Order product success');

            } else {

              return redirect()->back()
                ->with('error', 'Failed to order product');

            }

          }

        }

      }


    }

    /*
    * Order success
    */
    public function orderSuccess()
    {

      if (Session::get('order_id') == null) {
        return redirect('product');
      }

      $order = Order::find(Session::get('order_id'));
      return view('items.product.success')
        ->with('order', $order)
        ->with('message', $order->item->product->name .' that cost '. $order->total .' will be shipped to '. $order->item->product->shipping_address .'  after you pay');
    }

    /*
    * Create order number
    */
    protected function generateOrderNumber()
    {
      $on = mt_rand(1111111111, 9999999999);
      $check_on = Order::where('order_number', $on)->first();

      while ($check_on == $on)
      {
          $on = mt_rand(1111111111, 9999999999);
          $check_on = Order::where('order_number', $on)->first();
      }

      return $on;

    }
}
