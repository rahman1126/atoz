<?php

namespace App\Http\Controllers\Item;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;
use Session;
// use Queue;
use Auth;

use App\Pulsa;
use App\Order;
use App\Item;


class PulsaController extends Controller
{

    protected $itemFee = 5;
    protected $itemType = 'pulsa';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
    * Show order form
    */
    public function index()
    {
      return view('items.pulsa.buy');
    }

    /*
    * Store order
    */
    public function makeOrder(Request $request)
    {

      $valid = Validator::make($request->all(), [
        'phoneNumber'   => 'required|digits_between:7,12|regex:/(081)[0-9]/', // regex:/(081)[0-9]/
        'value'         => 'required|string',
      ]);

      if ($valid->fails()) {
        return redirect()->back()
          ->withErrors($valid)
          ->withInput();
      } else {

        $item = new Item;
        $item->user_id = Auth::id();
        $item->type = $this->itemType;

        if ($item->save()) {

          $pulsa = new Pulsa;
          $pulsa->item_id = $item->id;
          $pulsa->phone_number = $request->input('phoneNumber');
          $pulsa->value = $request->input('value');

          if ($pulsa->save()) {

            $order = new Order;
            $order->order_number = $this->generateOrderNumber();
            $order->item_id = $pulsa->item_id;
            $order->user_id = Auth::id();
            $order->total = $request->input('value') + ($request->input('value') * $this->itemFee / 100);

            if ($order->save()) {

              Session::put('order_id', $order->id);
              return redirect('prepaid-balance-success')
                ->with('success', 'Order pulsa success');

            } else {

              return redirect()->back()
                ->with('error', 'Failed to order pulsa');

            }

          }

        }

      }

    }

    /*
    * Order success
    */
    public function orderSuccess()
    {

      if (Session::get('order_id') == null) {
        return redirect('prepaid-balance');
      }

      $order = Order::find(Session::get('order_id'));
      return view('items.pulsa.success')
        ->with('order', $order)
        ->with('message', 'Your Mobile Phone Number '. $order->item->pulsa->phone_number .' will be topped up for '. number_format($order->item->pulsa->value, 0, ",", ".") .'  after you pay');
    }

    /*
    * Create order number
    */
    protected function generateOrderNumber()
    {
      $on = mt_rand(1111111111, 9999999999);
      $check_on = Order::where('order_number', $on)->first();

      while ($check_on == $on)
      {
          $on = mt_rand(1111111111, 9999999999);
          $check_on = Order::where('order_number', $on)->first();
      }

      return $on;

    }


}
