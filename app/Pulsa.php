<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pulsa extends Model
{
    protected $table = 'pulsa';
    protected $fillable = ['item_id','phone_number', 'balance_status', 'value'];

    public function item()
    {
      return $this->belongsTo('App\Item');
    }
}
