<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'items';
    protected $fillable = ['type'];

    public function order()
    {
      return $this->belongsTo('App\Order');
    }

    public function pulsa()
    {
      return $this->hasOne('App\Pulsa');
    }

    public function product()
    {
      return $this->hasOne('App\Product');
    }
}
