@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">

                  <div class="col-md-12">
                    <form class="form-inline pull-right" method="get" action="{{ url('order/search') }}">
                      <div class="form-group">
                        <div class="input-group">
                          <div class="input-group-addon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
                          <input type="text" class="form-control" name="key" placeholder="Search order number here..." value="{{ Request::get('key') }}">
                        </div>
                      </div>
                      <button type="submit" class="btn btn-primary">Search</button>
                    </form>
                  </div>

                  <br><br><br>

                  <div class="col-md-12">
                    <div class="panel panel-default">
                      <div class="panel-body">
                        <div class="col-md-2">
                          Order No.
                        </div>
                        <div class="col-md-4">
                          Description
                        </div>
                        <div class="col-md-3">
                          Total
                        </div>
                        <div class="col-md-3">
                          Information
                        </div>
                      </div>
                    </div>
                  </div>
                  @foreach ($orders as $order)
                    <div class="col-md-12">
                      <div class="panel panel-default">
                        <div class="panel-body">
                          <div class="col-md-2">
                            {{ $order->order_number }}
                          </div>
                          <div class="col-md-4">
                            @if ($order->item->type == 'pulsa')
                              {{ number_format($order->item->pulsa->value, 0, ',', '.') }} for {{ $order->item->pulsa->phone_number }}
                            @else
                              {{ $order->item->product->name }} that cost {{ number_format($order->item->product->price, 0, ',', '.') }}
                            @endif
                          </div>
                          <div class="col-md-3">
                            {{ number_format($order->total, 0, ',', '.') }}
                          </div>
                          <div class="col-md-3">
                            @if ($order->status == 0)
                              <a href="{{ url('payment?order=') . $order->order_number }}" class="btn btn-success">Pay</a>
                            @elseif($order->status == 2)
                              <span class="text-danger">Canceled</span>
                            @else
                              @if ($order->item->type == 'pulsa')
                                {{ $order->item->pulsa->balance_status }}
                              @else
                                Shipping Code : <strong>{{ $order->item->product->shipping_code }}</strong>
                              @endif
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                  @endforeach

                  <br>
                  <div class="col-md-12">
                    <div class="text-right">
                      {{ $orders->links() }}
                    </div>
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
