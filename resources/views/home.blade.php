@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-4">
                      Hello, {{ ( Auth::user()->name == '' ? Auth::user()->email : Auth::user()->name ) }}
                    </div>
                    <div class="col-md-4">
                      <a href="{{ url('prepaid-balance') }}" class="btn btn-info">Need a Prepaid Balance?</a>
                    </div>
                    <div class="col-md-4">
                      <a href="{{ url('product') }}" class="btn btn-info">Want to buy something?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
