@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="" method="post">
                      <div class="form-group {{ ( $errors->has('productName') ? 'has-error' : '' ) }}">
                        <label for="productName" class="form-label">Product</label>
                        <textarea name="productName" rows="3" cols="80" class="form-control">{{ old('productName') }}</textarea>
                        @if ($errors->has('productName'))
                          <span class="help-block">
                            <strong>{{ $errors->first('productName') }}</strong>
                          </span>
                        @endif
                      </div>
                      <div class="form-group {{ ( $errors->has('shippingAddress') ? 'has-error' : '' ) }}">
                        <label for="shippingAddress" class="form-label">Shipping Address</label>
                        <textarea name="shippingAddress" rows="3" cols="80" class="form-control">{{ old('shippingAddress') }}</textarea>
                        @if ($errors->has('shippingAddress'))
                          <span class="help-block">
                            <strong>{{ $errors->first('shippingAddress') }}</strong>
                          </span>
                        @endif
                      </div>
                      <div class="form-group {{ ( $errors->has('price') ? 'has-error' : '' ) }}">
                        <label for="price" class="form-label">Price</label>
                        <input type="text" name="price" value="{{ old('price') }}" class="form-control">
                        @if ($errors->has('price'))
                          <span class="help-block">
                            <strong>{{ $errors->first('price') }}</strong>
                          </span>
                        @endif
                      </div>
                      <div class="form-group text-right">
                        {{ csrf_field() }}
                        <button type="submit" name="button" class="btn btn-success">Submit</button>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
