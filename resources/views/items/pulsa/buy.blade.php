@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="" method="post">
                      <div class="form-group {{ ( $errors->has('phoneNumber') ? 'has-error' : '' ) }}">
                        <label for="phoneNumber" class="form-label">Mobile Phone Number</label>
                        <input type="text" name="phoneNumber" value="{{ old('phoneNumber') }}" class="form-control">
                        @if ($errors->has('phoneNumber'))
                          <span class="help-block">
                            <strong>{{ $errors->first('phoneNumber') }}</strong>
                          </span>
                        @endif
                      </div>
                      <div class="form-group {{ ( $errors->has('value') ? 'has-error' : '' ) }}">
                        <label for="value" class="form-label">Value</label>
                        <select class="form-control" name="value">
                          <option value="10000">10000</option>
                          <option value="50000">50000</option>
                          <option value="100000">100000</option>
                        </select>
                        @if ($errors->has('value'))
                          <span class="help-block">
                            <strong>{{ $errors->first('value') }}</strong>
                          </span>
                        @endif
                      </div>
                      <div class="form-group text-right">
                        {{ csrf_field() }}
                        <button type="submit" name="button" class="btn btn-success">Submit</button>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
