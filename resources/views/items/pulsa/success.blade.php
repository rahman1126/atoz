@extends('layouts.app')
@section('content')

<div class="container">
  <div class="row">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="text-center">
          <h4>Your Order Number</h4>
          {{ $order->order_number }}

          <br><br>
          <h4>Total</h4>
          {{ $order->total }}

          <br><br>
          <p>{{ $message }}</p>

          <br>
          <a href="{{ url('payment?order=') . $order->order_number }}"  class="btn btn-success btn-block">Pay Here</a>

        </div>
      </div>
    </div>
  </div>
</div>

@endsection
