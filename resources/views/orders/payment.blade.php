@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="" method="post">
                      <div class="form-group {{ ( $errors->has('orderNumber') ? 'has-error' : '' || Session::has('error') ? 'has-error' : '' ) }}">
                        <label for="orderNumber" class="form-label">Order Number</label>
                        <input type="text" name="orderNumber" value="{{ ( Request::get('order') != null ? Request::get('order') : old('orderNumber') ) }}" class="form-control">
                        @if ($errors->has('orderNumber'))
                          <span class="help-block">
                            <strong>{{ $errors->first('orderNumber') }}</strong>
                          </span>
                        @elseif (Session::has('error'))
                          <span class="help-block">
                            <strong>{{ Session::get('error') }}</strong>
                          </span>
                        @endif
                      </div>
                      <div class="form-group text-right">
                        {{ csrf_field() }}
                        <button type="submit" name="button" class="btn btn-success">Pay</button>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
