<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/order', 'OrderController@index')->name('order');

Route::get('/prepaid-balance', 'Item\PulsaController@index');
Route::post('/prepaid-balance', 'Item\PulsaController@makeOrder');
Route::get('/prepaid-balance-success', 'Item\PulsaController@orderSuccess');

Route::get('/product', 'Item\ProductController@index');
Route::post('/product', 'Item\ProductController@makeOrder');
Route::get('/product-success', 'Item\ProductController@orderSuccess');

Route::get('/payment', 'CheckoutController@getCheckout');
Route::post('/payment', 'CheckoutController@postCheckout');

Route::get('/order', 'OrderController@index');
Route::get('/order/search', 'OrderController@search');
